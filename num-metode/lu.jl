# A = [ 2  1  3 -4;
#      -4 -1 -4  7;
#       2  3  5 -3;
#      -2 -2 -7  8]
A = [2  8  1   3;
     5  1  9   1;
     4  1  1  10;
     1  2  2   6]
A = rand(1:10,(4,4))
U = copy(A)
L = Matrix(1I, size(U))
m, _ = size(U)
for k in 1:m-1
    ℓ = U[k+1:m,k] / U[k,k] # O(m-k)
    L[k+1:m,k] = ℓ
    U[k+1:m,k+1:m] += -ℓ * U[k,k+1:m]' # O(2(m-k)²)
end

# ∑_k=1:m-1 2(m-k)²

function find_A()
    while true
        try
            A = rand(1:10,(4,4))
            U = copy(A)
            L = Matrix(1I, size(U))
            m, _ = size(U)
            for k in 1:m-1
                ℓ = U[k+1:m,k] / U[k,k]
                L[k+1:m,k] = ℓ
                U[k+1:m,k+1:m] += -ℓ * U[k,k+1:m]'
            end
            return A
        catch InexactError
            @warn "Fail"
        end
    end
end
