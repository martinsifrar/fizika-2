% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{subsection}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

\title{Akustični resonator}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Dinamični opis klasičnega valovanja ponuja t. i. valovna enačba. V primeru zvoka za tlak\footnote{Ker ima okoliški zrak v vsakem primeru nek tlak $p_0$, je bolj naravno, da govorimo o relativnem tlaku $\delta z = p - p_0$. Ker predpostavimo, da $p_0$ ni funkcija časa ali prostora, je to tako ali tako brez pomena.}

\begin{equation*}
    \nabla^2 p = \frac{1}{c^2} \frac{\partial^2 p}{\partial t^2},
\end{equation*}

pri čemer je $c$ hitrost zvoka v zraku. To lahko izrazimo iz razmerja specifičnih toplot $\kappa$, gostote $\rho_0$ in okoliškega tlaka $p_0$

\begin{equation*}
    c = \sqrt{\frac{\kappa p_0}{\rho_0}}.
\end{equation*}

Če to enačbo rešimo za robne pogoje škatle z stranicami $A, B, C$, je lastna rešitev podana s tremi parametri, naravnimi števili $n_x, n_y, n_z$. Lastne krožna frekvence take škatle so

\begin{equation}
    \omega = c\pi \sqrt{\frac{n_x^2}{A^2} + \frac{n_y^2}{B^2} + \frac{n_z^2}{C^2}},
    \label{eq:freq}
\end{equation}

sam tlak pa je oblike

\begin{equation*}
    z = p - p_0 = z_0 \cos\left( \frac{n_x\pi x}{A} \right) \cos\left( \frac{n_y\pi y}{B} \right) \cos\left( \frac{n_z\pi z}{C} \right) \cos(\omega t + \varphi),
\end{equation*}

za neka nenegativna cela števila $n_x, n_y, n_z$, neko amplitudo $z_0$ in fazni zamik $\varphi$.

\section{Naloga}

\begin{enumerate}
    \item Izmeri resonančni odziv akustičnega resonatorja v območju od $\SI{200}{Hz}$ do $\SI{1000}{Hz}$ in ga nariši v ustrezen graf.
    \item Izračunaj najnižje resonančne frekvence akustičnega resonatorja za $n_i$ od 0 do 3 in dobljene frekvence (manjše od $\SI{1000}{Hz}$ v tabeli razvrsti po velikosti skupaj s pripadajočimi vrednostmi $(n_x, n_y, n_z)$. V tabeli pusti še dva prazna stolpca za izmerjene frekvence in amplitude.
    \item Primerjaj izmerjene in izračunane frekvence in na ta način določi, kateremu nihajnemu načinu pripadajo izmerjene resonance. Frekvence maksimumov in ustrezne amplitude vnesi v pripravljeno tabelo.
    \item Oceni razpolovno širino prvih treh resonančnih črt in še katere, ki je dovolj ločena od ostalih.
    \item Iz prvih treh resonanc izračunaj hitrost zvoka.
    \item Izmeri odvisnost signala od položaja mikrofona v škatli za osnovno in še nekatere višje resonance. Izberi si take frekvence, da bodo odvisnosti $p(\mathbf r)$ različne (recimo za $n_x$ od 0 do 3).
\end{enumerate}

\section{Meritve in račun}

\subsection{Resonančne frekvence}

Imamo resonator s stranicami dimenzij

\begin{align*}
    A = (56.7 \pm 0.1)\,\si{cm}, \\
    B = (38.5 \pm 0.1)\,\si{cm}, \\
    C = (24.0 \pm 0.1)\,\si{cm}. \\
\end{align*}

Po enačbi~\ref{eq:freq} lahko zdaj izračunamo resonančne frekvence, pri čemer za hitrost zvoka vzamemo

\begin{equation*}
    c^\mathrm{pre} = (434 \pm 2)\,\si{m/s}.
\end{equation*}

Vzamemo frekvence od $\SI{200}{Hz}$ do $\SI{1000}{Hz}$ in jih zapišemo v tabelo~\ref{tab:primerjava}. V tem frekvenčnem območju pomerimo tudi resonančno krivuljo. Merimo odziv, konkretno maksimalna amplitudo in povprečni kvadrat mikrofona. Odziv merimo tudi z drugačno metodo, kjer gledamo obnašanje, impedanco samega zvočnika (\textit{lock-in} metoda). Meritve so prikazane na sliki~\ref{fig:res-curves}.

\begin{figure}
    \centering
    \includegraphics{res-curves.pdf}
    \caption{Resonančne krivulje z vsemi tremi metodami -- razliko med maksimalim in minimalnim odzivom, povprečnim kvadratom (RMS) odmika in odmik z metodo \textit{lock-in}. Odmik je napetost, normalizirana z višino najvišjega vrha.}
    \label{fig:res-curves}
\end{figure}

Meritev ponovimo tudi z dušenjem s kosom blaga. Primerjavo resonančnih krivulj vidimo na~\ref{fig:muff}.

\begin{figure}
    \centering
    \includegraphics{muff.pdf}
    \caption{Resonančne krivulje z dušenjem in brez, povprečje treh metod. Oba odziva sta normalizirana z maksimalno amplitudo nedušenega odziva. Vidimo, da je dušena krivulja \blockquote{zglajena} in da so vsi vrhovi zamaknjeni nekoliko v desno.}
    \label{fig:muff}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{c|r|r r r|r|r}
        $(n_x, n_y, n_z)$ & $\nu^\mathrm{rač}\,[\si{Hz}]$ & $\nu^\mathrm{mer}\,[\si{Hz}]$ & $U_0^\mathrm{mer}$ & $\mathrm{FWHM}\,[\si{Hz}]$ & $c\,[\si{m/s}]$ & $\Delta \nu\,[\si{Hz}]$ \\
        \hline
        (1, 0, 0) & $302 \pm 2$ & 306 & 0.48 & 22 & $347.0 \pm 2.3$ & 4  \\
        (0, 1, 0) & $445 \pm 3$ & 446 & 0.48 & 46 & $343.4 \pm 1.6$ & 1  \\
        (1, 1, 0) & $538 \pm 3$ & 536 & 0.90 & 32 & $341.4 \pm 1.4$ & 2  \\
        (2, 0, 0) & $605 \pm 4$ & 602 & 1.00 & 20 & $341.3 \pm 1.7$ & 3  \\
        (0, 0, 1) & $715 \pm 4$ & 708 & 0.52 & 44 & $339.8 \pm 1.2$ & 7  \\
        (2, 1, 0) & $751 \pm 4$ & 750 & 0.82 & 30 & $342.4 \pm 1.6$ & 1  \\
        (1, 0, 1) & $776 \pm 5$ &     &      &    &                 &    \\
        (0, 1, 1) & $842 \pm 5$ & 840 & 0.66 &    & $342.2 \pm 1.2$ & 2  \\
        (0, 2, 0) & $891 \pm 5$ & 878 & 0.44 &    & $338.0 \pm 1.9$ & 13 \\
        (1, 1, 1) & $895 \pm 5$ & 904 & 0.83 & 24 & $346.6 \pm 1.2$ & 9  \\
        (2, 0, 1) & $936 \pm 5$ &     &      &    &                 &    \\
        (1, 2, 0) & $941 \pm 5$ & 942 & 0.81 &    & $343.4 \pm 2.0$ & 1
    \end{tabular}
    \caption{Izračunene resonance, izmerjene resonance ter odstopanje $\Delta\nu = |\nu^\mathrm{rač} - \nu^\mathrm{mer}|$. Pri izmerjenih vrhovih prazna mesta pomenijo, da težko določim, ali je pri dani izračunani resonanci res pripadajoč vrh. Posebej prazna mesta za razpolovno širino (FWHM) pomenijo, da vrh ni dovolj izrazit za preprosto določitev razpolovne širine. Glej sliko~\ref{fig:primerjava}.}
    \label{tab:primerjava}
\end{table}

Vse tri podatke (iz slike~\ref{fig:res-curves}) povprečimo in na dobljeni resonančni krivulji poiščemo resonance, vrhove odziva. Te zapišemo v tabelo~\ref{tab:primerjava}, kjer jih primerjavo. Pri $\SI{776}{Hz}$, kjer smo napovedali resonanco (1, 0, 1), vidimo ostro špičko, ki bi lahko bila napovedana resonanca. Poleg zadnji izmerjenem vrh pri $\SI{942}{Hz}$ izgleda, kot da vsebuje dva vrhova, torej resonanci (2, 0, 1) in (1, 2, 0). Vse frekvence za lažji pregled narišemo tudi na sliko~\ref{fig:primerjava}.

\begin{figure}
    \centering
    \includegraphics{peaks.pdf}
    \caption{Resonančne krivulje, povprečje treh metod. Rdeči trikotniki označujejo izmerjene, sive črte pa pričakovane, izračunane resonančne frekvence. Navpične sive črte označujejo napako izmerjenih frekvenc. Navpične zelene črte označujejo razpolovno širino (FWHM) najbolj izrazitih vrhov.}
    \label{fig:primerjava}
\end{figure}

Za izrazite vrhove lahko preprosto določimo t. i. FWHM, razpolovno širino vrha. V tabeli~\ref{tab:primerjava} vidimo izračunane vrednosti, na sliki~\ref{fig:primerjava} pa z zeleno označene navpične črte, ki te širine označujejo.

\subsection{Hitrost zvoka}

Iz izmerjenih vrhov lahko po formuli~\ref{eq:freq} zdaj izračunamo hitrosti zvoka. Pri tem za napako resonančne frekvence vzamemo kar $\SI{2}{Hz}$. Izračunane hitrosti vidimo v tabeli~\ref{tab:primerjava}. Iz vseh hitrosti lahko izračunamo obteženo povprečje

\begin{equation*}
    c^\mathrm{rač} = \frac{1}{N} \sum_{i} \frac{c_i}{(\Delta c_i)^2},
\end{equation*}

pri čemer je $\Delta c_i$ napaka posameznega $c_i$, $N$ pa število izračunanih hitrosti. Za napako izračunanega $c^\mathrm{rač}$ vzamemo povprečje posameznih $\Delta c$ in to delimo z $\sqrt{N}$. Izračunamo, da je hitrost zvoka torej

\begin{equation*}
    c^\mathrm{rač} = (342.5 \pm 0.5)\,\si{m/s}.
\end{equation*}

\subsection{Profil zvoka}

Mikrofonu zdaj spreminjamo položaj $x$ (vzdolž stranice $A$). Na vsak centimeter izmerimo odziv. Krajevno odvisnost treh nihanjnih načinov lahko vidimo na sliki~\ref{fig:profili}.

\begin{figure}
    \centering
    \includegraphics{profiles.pdf}
    \caption{Krajevni profil odziva znotraj škatle za tri različne resonance.}
    \label{fig:profili}
\end{figure}

\subsection{Komentar glede meritev}

Pri meritvah sem se sprva soočil s problemom, da so bilo nekateri resonančni vrhovi odrezani pri vrednosti 1. To je pokvarilo tudi meritve zvočnega profila. Po nekaj neuspešnega preiskušanja z raznimi oblikami dušenja (kapa, šal), sem ugotovil, da je problem v nastavitvi potenciometra, ki mikrofon povezuje z računalnikom.

\end{document}
