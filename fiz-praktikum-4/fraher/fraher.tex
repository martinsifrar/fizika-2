% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

\title{Franck-Herzov poskus}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Diskretnost energijskih nivojev elektronov v atomu lahko pokažemo s Franck-Hertzovim
poskusom. Leta 1914 sta poskus opravila nemška fizika James Franck in Gustav Hertz. Plinska trioda (slika~\ref{fig:circuit}) vsebuje kapljico živega srebra $\mathrm{Hg}$, plinska faza nad njo pa ima pri temperaturi $\SI{200}{\celsius}$ tlak okoli $\SI{1}{kPa}$. V cevi pospešujemo elektrone od katode proti anodni mrežici z napetostjo $U_1$ in jih nato lovimo s kolektorsko anodo, ki elektrone dodatno odbija z majhnim potencialom $U_2$. Merimo tok elektronov $I_2$, ki doseže kolektorsko anodo, to je tok elektronov, ki uspejo premagati zaustavitveni potencial $U_2$ med anodno mrežico in anodnim kolektorjem.

\begin{figure}[ht]
    \centering
    \includegraphics{circuit.pdf}
    \caption{Skica triode v Frack-Herzovem poskusu. Od leve katode (emitorja) do anodne mrežice elektron pospešuje v polju napetosti $U_1$. Med anodno mrežico in anodnim kolektorjem je polje obrnjeno v drugo smer -- elektron mora za stik s katodo premagati napetost $U_2$.}
    \label{fig:circuit}
\end{figure}

Ko povečujemo napetost $U_1$, s katero pospešujemo elektrone, doseže kolektorsko anodo vedno več elektronov. A ko kinetične energije elektronov dosežejo $\SI{4.9}{eV}$ -- razliko $\Delta E = E_1 - E_0$ med prvima dvema vzbujenima stanjema $\mathrm{Hg}$ atoma -- postanejo nekateri trki neelastični. Posledično se elektroni upočasnijo in ne dosežejo kolektorske anode. V odvisnosti toka $I_2$ vidimo značilen padec. Pri višjih napetostih, npr. $\SI{9.8}{V}$, imajo elektroni že na sredini pospeševalnega pasu kinetično energijo $\SI{4.9}{eV}$. To je dovolj, da jo izgubijo v neelastičnem trku. Od tukaj do anode mrežice spet pridobijo energijo $\SI{4.9}{eV}$ in drugič neelastično trčijo tik ob anodni mrežici. Spet torej opazimo padec v kolektorskem toku $I_2$.

\section{Naloga}

\begin{enumerate}
    \item Opazuj odvisnost toka $I_2$ med anodno mrežico in anodnim kolektorjem v odvisnosti od negativne napetost $U_1$ na katodi. Spreminjaj temperaturo in posebej natančno opazuj in izmeri položaje vseh vrhov v merjenih odvisnostih. Skiciraj odvisnosti pri petih različnih temperaturah, ko se slike primerno razlikujejo, približno pri 180, 160, 140 in $\SI{120}{\celsius}$ in na koncu še pri sobni temperaturi.
    \item Natančno določi položaje vrhov $U_{1,n} = U_2 + n\Delta E/e_0$ pri posameznih temperaturah in rezultate vnesi v tabelo. Razlike napetosti med zaporednimi maksimumi ustrezajo energiji, ki jo izgubijo elektroni pri posameznem neelastičnem trku z atomom $\mathrm{Hg}$. Določi $\Delta E = E_1 - E_0 = e_0 \Delta U_1$, kjer sta $E_1$ in $E_0$ energiji prvega vzbujenega in osnovnega stanja elektrona v zunanji lupini $\mathrm{Hg}$.
\end{enumerate}

\section{Meritve in račun}

Prižgemo komoro, da se bučka z živim srebrom začne segrevati. Nato pri temperaturah 180, 160, 141, 120 in še $\SI{33}{\celsius}$ pomerimo odvisnosti $U_2(U_1)$ (sliki~\ref{fig:meas-1},~\ref{fig:meas-2}). Pri tem je $U_2$ le napetost, ki je preko upora $R = \SI{1}{k\ohm}$ povezana s kolektorskim tokom $I_2 = U_2/R$.

Iz grafov razberemo relativne\footnote{Merimo jih od najnižje napetosti na slikah~\ref{fig:meas-1},~\ref{fig:meas-2}.} višine maksimumov kolektorskega toka $I_2$ in pripadajoče pospeševalne napetosti $U_1$ (slika~\ref{fig:I_2-by_U_1}). Iz tega lahko nato izračunamo, da so razmiki med vrhovi

\begin{equation*}
    \Delta E = (5.0 \pm 0.1)\,\si{eV}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{I_2-by_U_1.pdf}
    \caption{Tok na kolektorski katodi v odvisnosti od napetosti $U_1$ med emitorsko katodo in anodno mrežico. Odvisnost je izmerjena pri temperaturah 120, 141, 160 in $\SI{180}{\celsius}$.}
    \label{fig:I_2-by_U_1}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{measurements/TEK0010.JPG}
    \includegraphics{measurements/TEK0008.JPG}
    \includegraphics{measurements/TEK0006.JPG}
    \caption{Slike $U_2(U_1)$ pri temperaturah 33, 120 in $\SI{141}{\celsius}$.}
    \label{fig:meas-1}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{measurements/TEK0001.JPG}
    \includegraphics{measurements/TEK0003.JPG}
    \caption{Slike $U_2(U_1)$ pri temperaturah 160 in $\SI{180}{\celsius}$.}
    \label{fig:meas-2}
\end{figure}

\end{document}
