% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

\title{Določitev osnovnega naboja po Millikanu}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Pri Millikanovem poskusu opazovujemo gibanja naelektrenih kapljic v gravitacijskem in električnem polju. Poskus je zgodovinskega pomena, saj sta z njim Robert Millikan in Harvey Fletcher v 1908 prvič izmerila osnovni električni naboj.

Na naelektreno okroglo kapljico, ki pada v zraku, delujejo sila teže, električna sila in sila upora. Po kratkem relaksacijskem času se kapljica giblje premo enakomerno. Če gravitacijski pospešek $g$, električno polje $E$ in hitrost padanja $v$ merimo v isti smeri, velja

\begin{equation}
    \frac{4\pi(\rho - \rho_\mathrm{zr})g}{3} r^3 + qE - 6\pi \eta_\mathrm{zr} rv = 0
    \label{eq:main}
\end{equation}

pri čemer je $r$ radij kapljice, $q$ njen naboj, $\rho$, $\rho_\mathrm{zr}$ gostoti olja in zraka ter $\eta_\mathrm{zr}$ viskoznost zraka. Enačbo~(\ref{eq:main}) pri različnih za isto kapljico izmerjenih vrednostih $E$ in $v$ -- torej dve enačbi -- lahko seštejemo v

\begin{equation*}
    \frac{8\pi(\rho - \rho_\mathrm{zr})g}{3} r^3 + q(E_1 + E_2) - 6\pi \eta_\mathrm{zr} r(v_1 + v_2) = 0.
\end{equation*}

Ker bomo pri eksperimentu polje spremenili tako, da ohranimo $|E|$ in le obrnemo njegovo smer. Torej velja $E_1 + E_2 = 0$ in radij kapljice lahko izračunamo kot

\begin{equation}
    r = \frac{3}{2} \sqrt{\frac{\eta_\mathrm{zr} (v_1 + v_2)}{g(\rho - \rho_\mathrm{zr})}}.
    \label{eq:r}
\end{equation}

Podobno lahko izraza za različne vrednostih $E$ in $v$ v enačbi~(\ref{eq:main}) odštejemo. Upoštevamo še, da je jakost polja izražena z napetostjo med elektrodama $U$ in razdaljo med elektrodama $d$. Naboj kapljice lahko izračunamo kot

\begin{equation}
    q = \frac{3\pi d\eta_\mathrm{zr}}{U}\,r|v_2 - v_1|.
    \label{eq:q}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Izmeri hitrosti gibanja kapljic v gravitacijskem in električnem polju.
    \item Iz meritev izračunaj velikosti kapljic in njihov naboj ter določi osnovni naboj.
\end{enumerate}

\section{Meritve in račun}

Za račun bomo potrebovali tudi gostoto olja, zraka ter njegovo viskoznost, zato jih navedimo na začetku

\begin{align*}
    \rho &= (973 \pm 1)\,\si{kg/m^3}, \\
    \rho_\mathrm{zr} &= (120 \pm 5)\,\si{kg/m^3}, \\
    \eta_\mathrm{zr} &= (18.3 \pm 1)\,\si{\mu s Pa}.
\end{align*}

Prižgemo visokonapetostni vir in na elektrodi kondenzatorja pritisnemo napetost

\begin{equation*}
    U = (245 \pm 2)\,\si{V}.
\end{equation*}

Vemo tudi, da sta elektrodi na razdalji

\begin{equation*}
    d = (5 \pm 0.1)\,\si{mm}.
\end{equation*}

Prižgemo kamero in komoro v kondenzatorju osvetlimo. V komoro vbrizgamo kapljice in posnamemo, kako se premikajo. Sredi posnetka prek stikala obrnemo smer polja, tako da se kapljice upočasnijo ali začnejo gibati nasproto smer. S programom \verb|TWview| odpremo posnetek in poiščemo majhne kapljice, ki se gibajo dovolj počasi, da jim brez težav sledimo. Izmerimo, kako daleč se premakne kapljica -- prvo v polju $E > 0$, nato v polju $E < 0$. Ključno je, da shranimo premika v $x$ in $y$ smeri ter čas, ki ga je kapljica porabila za ta premik.

Iz teh podatkov lahko za vsako kapljico izračunamo hitrosti $v_1$, $v_2$. Za podrobnosti glej \ref{sec:smer}. Preko~(\ref{eq:r}) in~(\ref{eq:q}) izračunamo radij kappljic in naboj posameznih kapljic ter jih zapišemo v tabelo~\ref{tab:r-q}.

Vse naboje predstavimo na kumulativnem histogramu~\ref{fig:N-by-q}, iz kjer razberemo, da je vrednost osnovnega naboja okoli

\begin{equation*}
    q_0 = (1.5 \pm 0.3) \cdot 1.602 \cdot 10^{-19}\,\si{As},
\end{equation*}

kar je ravno $1.5 \pm 0.3$ pričakovane vrednosti osnovnega naboja. To sistematsko napako sem poskusil, a neuspešno, rešiti s smernim popravkom, ki je opisan v nadaljevanju.

\begin{table}
    \centering
    \begin{tabular}{r r|r r}
        $v_1\,[\si{\mu m/s}]$ & $v_2\,[\si{\mu m/s}]$ & $r\,[\si{nm}]$ & $q\,[1.602 \cdot 10^{-19}\,\si{As}]$ \\
        \hline
        $98.6$ & $-43.6$ & $520$ & $0.03$ \\
        $95.0$ & $-30.7$ & $562$ & $0.08$ \\
        $92.2$ & $-23.2$ & $583$ & $0.08$ \\
        $24.7$ & $22.2$ & $480$ & $1.28$ \\
        $120.7$ & $-88.1$ & $401$ & $1.43$ \\
        $125.2$ & $-90.6$ & $412$ & $1.46$ \\
        $66.5$ & $62.2$ & $796$ & $1.47$ \\
        $97.8$ & $22.1$ & $768$ & $1.48$ \\
        $91.4$ & $-30.5$ & $548$ & $1.52$ \\
        $99.0$ & $-0.7$ & $696$ & $1.55$ \\
        $102.1$ & $-54.3$ & $485$ & $1.63$ \\
        $91.1$ & $-30.9$ & $544$ & $1.67$ \\
        $25.8$ & $17.5$ & $462$ & $1.84$ \\
        $93.5$ & $-5.3$ & $659$ & $1.95$ \\
    \end{tabular}
    \caption{Hitrosti $v_1$, $v_2$ (izračunane s smernim popravkom), izračunan radij kapljic ter izračunan naboj v enotah, ki ustrezajo pričakovani vrednosti osnovnega naboja $q_0$.}
    \label{tab:r-q}
\end{table}

\begin{figure}
    \centering
    \includegraphics{N-by-q.pdf}
    \caption{Kumulativni histogram. Na vodoravni osi imamo vrednost izmerjenega naboja $q$, na navpični pa število izmerjenih nabojev $N$, katerih vrednost je manjša od naboja $q$. Pri vrednosti naboja, kjer ima histogram največjo strmino, predvidevamo, da je vrednost naboja celoštevilski večkratnik osnovnega naboja, torej nek $q = m q_0$. Tu je edini tak večkratnik kar $m = 1$, vidimo pa tudi nekaj kapljic, za katere izgleda, da sploh niso bile nabite.}
    \label{fig:N-by-q}
\end{figure}


\subsection{Smerni popravek}
\label{sec:smer}

Za enačbi~(\ref{eq:r}) in~(\ref{eq:q}) potrebujemo hitrosti kapljice v smeri gravitacije, predznačena skalarja $v_1$, $v_2$. Prvo sem gledal kar absolutni vrednosti $|\mathbf v_1|$, $|\mathbf v_2|$, pomnoženi s predznakom premika v navpični smeri. Ker so bili tako izračunani naboji nekoliko previsoki, sem to poskusil rešiti z smernim popravkom. Točna smer električnega in gravitacijskega polje je namreč neznana (kamera je nekoliko postrani). Poleg tega so se na posnetku kapljice gibale tudi v prečni smeri. Za popravek sem iz vektorskih hitrosti izračunal glavno smer, normiran vektor $\mathbf n$ (glej sliko~\ref{fig:v-directions}), nato pa vektorske hitrosti projiciral na $\mathbf n$.

\begin{equation*}
    v_1 = \mathbf n \cdot \mathbf v_1, \qquad v_1 = \mathbf n \cdot \mathbf v_1.
\end{equation*}

Izračunani naboji so kljub temu previsoki, torej sklepam na nek drug izvor sistematske napake. Če bi imel možnost ponoviti eksperiment, bi preveril, ali je problem v faktorju, s katerim pomnožimo na posnetku izmerjeno razdaljo v pikslih, da jo pretvorimo v enote prostorske razdalje.

\begin{figure}
    \centering
    \includegraphics{v-directions.pdf}
    \caption{Smeri izmerjenih hitrosti. Normalizirane hitrosti za $E > 0$ in $E < 0$. Smerni vektor $\mathbf n$ dobimo s povprečenjem $\operatorname{sgn} (v_x) \mathbf v$.}
    \label{fig:v-directions}
\end{figure}

\end{document}
