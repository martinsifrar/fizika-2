% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

\title{Spektrometer}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Spektroskop je priprava za merjenje spektrov, to je porazdelitev svetlobnega toka po frekvenci oz. valovni dolžini. V naši vaji bomo uporabljali klasični optični spektroskop na prizmo, ki temelji na principu disperzije -- ker je lomni količnik odvisen od valovne dolžine, se svetloba se v prizmi iz stekla razcepi na raznobarvne komponente. Disperzijo, s kakršno imamo opravka v prizmi spektroskopa, dobro opisuje Sellmeierjeva formula

\begin{equation*}
    n^2 = 1 + \frac{A}{1 - \left( \frac{\lambda}{\lambda_0} \right)^2},
\end{equation*}

kjer je $A$ t. i. moč oscilatorja in $\lambda_0$ valovna dolžina njegove resonance.

\section{Naloga}

\begin{enumerate}
    \item\label{it:1} Umerite kotno skalo spektroskopa s spektralnimi črtami Hg in $\rm{H_2}$
    \item Izmerite valovne dolžine spektralnih črt v spektru varčne žarnice. Primerjajte spekter s tistim, izmerjenim v Hg pod točko~\ref{it:1}.
    \item Izmerite centralno valovno dolžino in ocenite spektralno širino rdeče, rumene, zelene in modre svetleče diode (LED).
    \item Opazujte zvezni spekter volframove žarnice in oceni valovno dolžino najsvetlejšega (rumenega) dela in zapišite intervale, ki jih pokrivajo posamezne barve.
    \item Opazujte absorpcijski spekter NO2 tako, da cevko s plinom presevate z belo svetlobo.
    \item Izmerite valovne dolžine črt v spektru He in Ne.
\end{enumerate}

\section{Meritve in račun}

Za bučko z živim srebrom in bučko z vodikom izmerimo kote in preko barv z njimi asociiramo valovne dolžine najbolj močnih emisijskih črt (tabeli~\ref{tab:Hg-H2}). Da kalibriramo spektroskop, na te podatke prilagodimo krivuljo oblike

\begin{equation}
    \phi(\lambda) = A\lambda + B\sqrt{\lambda} + C.
    \label{eq:phi-model}
\end{equation}

Izračunamo parametre

\begin{align*}
    A &= (30 \pm 20) \cdot 10^6\,\si{\degree/m}, \\
    B &= (-60 \pm 30) \cdot 10^3\,\si{\degree/m^{1/2}}, \\
    C &= (70 \pm 10) \cdot 10^3\,\si{\degree}.
\end{align*}

Napake teh parametrov so zelo velike. To lahko nazorno na sliki~\ref{fig:phi-calibration}, kjer imamo poleg fita narisano tudi eno izbiro parameterov $A$, $B$, $C$ iz roba $1\sigma$ območja. Kljub temu, da so parametri $1\sigma$ odmaknjeni od sredinskih vrednosti, je krivulja še vedno zelo podobna dejanskemu fitu s sredinskimi vrednostmi parametrov.

\begin{table}
    \centering
    \begin{tabular}{c}
        živo srebro (Hg) \\
        \begin{tabular}{c|r r}
            \hline
            barva & $\phi\,[\si{\degree}]$ & $\lambda_\mathrm{pr}\,[\si{nm}]$ \\
            \hline
            2 rumeni & 38.2 & 578 \\
            zelena & 38.5 & 546 \\
            vijolična & 39.9 & 434
        \end{tabular} \\
        \\
        vodik ($\rm{H_2}$) \\
        \begin{tabular}{c|r r}
            \hline
            barva & $\phi\,[\si{\degree}]$ & $\lambda_\mathrm{pr}\,[\si{nm}]$ \\
            \hline
            rdeča & 36.2 & 656
        \end{tabular} \\
    \end{tabular}
    \caption{Opažene barve emisijskih črt živega srebra in vodika, pripadajoči koti in asociirane (pričakovane) valovne dolžine emisijskih črt.}
    \label{tab:Hg-H2}
\end{table}

\begin{figure}
    \centering
    \includegraphics{phi-calibration.pdf}
    \caption{Izmerjeni koti in asociirane valovne dolžine za živo srebro in vodik. Poleg fita je pikčasto narisana krivulja s parametri, ki so zamaknjeni za $1\sigma$.}
    \label{fig:phi-calibration}
\end{figure}

Poleg živega srebra in vodika pomerimo tudi varčno žarnico, različne LED diode in bučki s helijem in neonom. Če iz~(\ref{eq:phi-model}) izrazimo

\begin{equation*}
    \lambda(\phi) = \left( \frac{-B + \sqrt{B^2 + 4A (C - \phi)}}{2A} \right)^2,
\end{equation*}

lahko iz kotov izračunamo valovne dolžine~(tabeli~\ref{tab:other},~\ref{tab:LED} ter slike~\ref{fig:incandescent-spectrum},~\ref{fig:LED-spectra} in \ref{fig:He-Ne-spectrum}.)

\begin{table}
    \centering
    \begin{tabular}{c}
        varčna žarnica \\
        \begin{tabular}{c|r r}
            \hline
            barva & $\phi\,[\si{\degree}]$ & $\lambda_\mathrm{pr}\,[\si{nm}]$ \\
            \hline
            vijolična & 38.0 & 601 \\
            moder prehod & 38.4 & 556 \\
            zelena B & 38.4 & 556 \\
            zelena A & 38.5 & 546 \\
            rdeča & 39.1 & 492
        \end{tabular} \\
        \\
        helij ($\rm{H_2}$) \\
        \begin{tabular}{c|r r}
            \hline
            barva & $\phi\,[\si{\degree}]$ & $\lambda_\mathrm{pr}\,[\si{nm}]$ \\
            vijolična & 38.0 & 601 \\
            svetlomodra & 38.8 & 518 \\
            oranžna & 39.6 & 455
        \end{tabular} \\
        \\
        neon (Ne) \\
        \begin{tabular}{c|r r}
            \hline
            barva & $\phi\,[\si{\degree}]$ & $\lambda_\mathrm{pr}\,[\si{nm}]$ \\
            \hline
            rumena & 37.7 & 641 \\
            oranžna & 37.9 & 614 \\
            rdeča & 38.1 & 589
        \end{tabular} \\
    \end{tabular}
    \caption{Opažene barve emisijskih črt varčne žarnice, helija in neona, pripadajoči koti in izračunane valovne dolžine emisijskih črt.}
    \label{tab:other}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{c|r r|r r}
        \hline
        barva diode & $\phi_\mathrm{min}\,[\si{\degree}]$ & $\phi_\mathrm{max}\,[\si{\degree}]$ & $\lambda_\mathrm{min}\,[\si{nm}]$ & $\lambda_\mathrm{max}\,[\si{nm}]$ \\
        \hline
        rdeča & 37.5 & 37.9 & 614 & 672 \\
        zelena & 37.8 & 38.3 & 567 & 627 \\
        rumena & 37.7 & 38.3 & 567 & 641 \\
        modra & 38.6 & 39.6 & 455 & 536 \\
    \end{tabular}
    \caption{Razpon kotov in izračunan razpon valovnih dolžin emisijskih območij za LED diode različnih barv.}
    \label{tab:LED}
\end{table}

\begin{figure}
    \centering
    \includegraphics{incandescent-spectrum.pdf}
    \caption{Spekter varčne žarnice.}
    \label{fig:incandescent-spectrum}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{LED-spectra.pdf}
    \caption{Spektri LED diod različnih barv.}
    \label{fig:LED-spectra}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{He-spectrum.pdf}
    \includegraphics{Ne-spectrum.pdf}
    \caption{Spektra helija in neona.}
    \label{fig:He-Ne-spectrum}
\end{figure}


\begin{figure}
    \centering
    \includegraphics{thermal-spectrum.pdf}
    \caption{Spekter žarnice na volframovo nitko, razdeljen po barvah in z valovno dolžino največje jakosti (ocenjeno s prostim očesom).}
    \label{fig:thermal-spectrum}
\end{figure}

\end{document}
