% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

\title{Dielektrična anizotropija tekočega kristala}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

V nekaterih snoveh poznamo med tekočo in kristalno še tekočekristalno fazo. En preprostejših primerov je nematična faza, ki jo opazimo v snoveh s podolgovatimi mulekulami. V nematični fazi snov še vedno teče kot tekočina, a orientacije posameznih mulekul so korelirane. Preferenčno smer orientacije mulekul označimo z direktorjem -- enotskim vektorjem $\hat{\mathbf{n}}$. Smeri $\hat{\mathbf{n}}$ in $-\hat{\mathbf{n}}$ sta enakovredni. Urejenost opišemo s skalarnim parametrom $\mathcal S \in [0, 1]$, definiranim kot

\begin{equation*}
    \mathcal S \das \frac{1}{2} \int_{-1}^1 \left( \cos^2\varphi - 1 \right) f(\varphi) \,\diff (\cos\varphi),
\end{equation*}

pri čemer je $\varphi$ kot med direktorjem in orientacijo mulekul, $f(\varphi)$ pa porazdelitev mulekul po temu kotu. Razlika med z direktorjem vzporedno in nanj pravokotno komponento dielektričnega tenzorja takega tekočega kristala je sorazmerna z urejenostjo

\begin{equation*}
    \varepsilon_\parallel - \varepsilon_\perp \propto \mathcal S.
\end{equation*}

Poleg prehoda, ki ga povzroči temperatura, lahko v tekočih kristalih opazujemo tudi fazni prehod, ki a pozdroči zunanje električno polje. Konkretno obravnavamo Frederiksov prehod. Tekoči kristal imamo v kondenzatorju, v katerem elektrodi direktorju vsiljujeta smer, pravokotno na polje, torej v ravnini elektrod. V takem sistemu temujeta dva faktorja. Reorientacija direktorja v smeri vzdolž polja energije zmanjša za $\Delta\varepsilon\varepsilon_0 E^2$. Po drugi strani reorientacija v smeri polja pomeni rotacijsko mehanično deformacijo in torej povečanje enerije za $K_1 (\pi/d)^2$. Prehod, kjer se delci res orientacija v smeri polja, se zgodi na začetku napetostnega območja, kjer je vsota obeh členov manjša od nič. Velja

\begin{equation}
    U_c = \pi \sqrt{\frac{K_1}{\Delta\varepsilon\varepsilon_0}},
    \label{eq:U_c}
\end{equation}

kjer je $K_1$ orientacijska elastična konstanta, upoštevali pa smo $E = U/d$.

\section{Naloga}

\begin{enumerate}
    \item Izmerite temperaturno odvisnost komponent dielektričnega tenzorja vzdolž in pravokotno na direktor.
    \item Narišite na isti graf temperaturno odvisnost obeh komponent dielektričnega tenzorja in povprečne dielektrične konstante. Narišite graf dielektrične anizotropije v odvisnosti od temperature. Določite temperaturo faznega prehoda.
    \item Izmerite odvisnost kapacitivnosti celice z orientacijo direktorja vzdolž stekelc od napetosti, narišite graf $C(U)$ ter določite elastično konstanto tekočega kristala.
\end{enumerate}

\section{Meritve in račun}

\subsection{Izotropno-nematični prehod}

Merimo vzporedno in pravokotno kompenento dielektrične tenzorja, $\varepsilon_\parallel$ in $\varepsilon_\perp$. Prvo izmerimo v kondenzatorju, kjer je orientacija mulekul vsiljeno pravokotna na plošči, drugo pa v kondenzatorju, kjer je njihova orientacija vsiljeno v ravnini plošč. Začetne kapacitivnosti teh dveh kondeznatorjev sta

\begin{align*}
    C_{0,\parallel} &= \SI{53.4}{pF}, \\
    C_{0,\perp} &= \SI{56.5}{pF}.
\end{align*}

Kapacitivnost kondenzatorjev merimo z $LCR$ metrom, temperaturo pa z umeritvijo uporovnega termometra PT100. Izmerjeni kapacivnosti sta preprosto za relativno dieletričnost -- torej komponenti dieletričnega tenzorja $\varepsilon_\parallel$, $\varepsilon_\perp$ -- večja od začetnih kapacitivnosti $C_{0,\parallel}$ in $C_{0,\perp}$. Komponenti narišemo skupaj na en graf~(slika~\ref{fig:anisotropy}).

\begin{figure}
    \centering
    \includegraphics{eps-by-T.pdf}
    \caption{Izmerjeni komponenti dielekričnega tenzorja. Meritve $\varepsilon_\parallel$ so kubično interpolirane, tako da v vzorčenju temperatur sovpadajo z meritvami $\varepsilon_\perp$.}
    \label{fig:eps-by-T}
\end{figure}

Z interpolacijo pri istih temperaturah izračunamo povprečje $(\varepsilon_\parallel + \varepsilon_\perp)/2$~(slika~\ref{fig:eps-by-T}) in anizotropnost $\Delta\varepsilon = \varepsilon_\parallel - \varepsilon_\perp$~(slika~\ref{fig:anisotropy}). Iz slednje najlažje razberemo temperaturo faznega prehoda

\begin{equation*}
    T_c = (331.0 \pm 0.3)\,\si{K} = (57.9 \pm 0.3)\,\si{\celsius}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{anisotropy.pdf}
    \caption{Anizotropija $\varepsilon_\parallel - \varepsilon_\perp$ tekočega kristala in prehod iz izotropne v nematično fazo. $T_c$ vzamemo nekoliko arbitrarno kot sredino območja temperature, kjer krivulja najhitreje narašča. Še nekoliko bolj arbitrarno vzamemo za napako kritične temperature četrtino širine tega območja.}
    \label{fig:anisotropy}
\end{figure}

\subsection{Frederiksov prehod}

Da se naš tekoči kristal obnaša kot idealni kondenzator, ga moramo napajati z izmeničnim tokom dovolj visoke frekvence. Namesto konstantne napetosti govorimo o ekvivalentni RMS napetosti $U$ in RMS toku $I$.

Z osciloskopom spremljamo napetosti na celici ter napetost na znanem uporu

\begin{equation*}
    R = \SI{1}{k\ohm},
\end{equation*}

preko katerega poznamo tok skozi vezje, kar je tudi tok na kondenzatorju. Preko tega lahko izračunamo kapacitivnost celice pri tej RMS napetosti (slika~\ref{fig:C-by-U}). Podobno kot pri izotropno-nematičnem prehodu razberemo kritično napetost, pri kateri se prehod zgodi

\begin{equation*}
    U_c = (2.0 \pm 0.2)\,\si{V},
\end{equation*}

in pripadajočo kritično kapacitivnost

\begin{equation*}
    C_c = (7.2 \pm 0.4)\,\si{\mu F}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{C-by-U.pdf}
    \caption{Odvisnost kapacitivnosti kondenzatorja s tekočeim kristalom od napetosti in fazni prehod v najhitreje naraščajočem delu krivulje. $U_c$ vzamemo kot sredino območja napetosti, kjer krivulja najhitreje narašča. Za napako kritične napetosti vzamemo četrtino širine tega območja. Na enak način iz $y$-osi razberemo kritično kapacitivnost $C_c$.}
    \label{fig:C-by-U}
\end{figure}

Ker poznamo dimenzije kondenzatorja

\begin{align*}
    S &= \SI{4}{cm^2}, \\
    d &= \SI{20.6}{\mu m},
\end{align*}

lahko izračunamo $\varepsilon_\parallel$ tekočega kristala, kar je kar približno anizotropija $\Delta\varepsilon$ v visoko anizotropnem režimu. Po~(\ref{eq:U_c}) lahko torej izračuamo rotacijsko elastično konstanto kristala

\begin{equation*}
    K_1 = (1.4 \pm 0.3) \cdot 10^{-7}\,\si{kg m/s^2}.
\end{equation*}

\end{document}
