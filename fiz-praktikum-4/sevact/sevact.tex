% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

\title{Sevanje črnega telesa}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Spektralni izsev črnega telesa, je pri konstantni temperaturi $T$ podan s Planckovim zakonom

\begin{equation}
    B(T; \nu) = \frac{2h\nu^3}{c^2} \frac{1}{e^{h\nu/kT} - 1},
    \label{eq:planck}
\end{equation}

tako da je $BS \,\diff\Omega\,\diff\nu$ diferencialna moč, izsevana skozi prostorski kot $\diff\Omega$, če je $S$ sevalna površina črnega telesa. Za nezastrt vir svetlobe lahko z integracijo po frekvenci in prostorskem kotu izpeljemo Stefan-Boltzmanov zakon

\begin{equation}
    \frac{P}{S} = \sigma T^4,
    \label{eq:stefan}
\end{equation}

ki podaja moč, izsevano iz površine $S$ v celoten prostor.

Prepustnost silicijevega filter, s katerim v drugem delu eksperimenta zastremo žarnico, lahko precej dobro opišemo kot stopničasto funkcijo, ki prepušča vse fotone z energijo manjšo kot $E_0 = \SI{1.1}{eV}$. Delež skozi silicijev filter prepuščene moči, če upoštevamo le izgube znotraj kristalne mreže, podaja analitično nerešljiv integral

\begin{equation*}
    \frac{P'_\mathrm{Si}}{P} = \int_0^{E_0/kT} \frac{y^3}{e^y - 1} \,\diff y,
\end{equation*}

ki pa ga lahko za temperature manjše od $\SI{3000}{K}$ na 3 decimalke ocenimo z

\begin{equation*}
    \frac{P'_\mathrm{Si}}{P} \approx 1 - \frac{15}{\pi^4} \left[ \ln(1 - e^{-y}) + e^{-y} \left( 3y^2 + 6y + 6 \right) \right].
\end{equation*}

Zaradi loma na površinah filtra imamo še dodatne izgube, ki jih lahko prestavimo s preprostim faktorjem

\begin{equation}
    \eta = \frac{2n}{n^2 + 1},
    \label{eq:n}
\end{equation}

tako da je skozi filter zares prepuščena moč $P_\mathrm{Si} = \eta P'_\mathrm{Si}$. Pri tem je $n$ lomni količnik. Prepustnost definiramo kot

\begin{equation*}
    t \das \frac{P_\mathrm{Si}}{P},
\end{equation*}

iz česar sledi, da je prepustnost oblike

\begin{equation}
    t = \eta \left\{ 1 - \frac{15}{\pi^4} \left[ \ln(1 - e^{-y}) + e^{-y} \left( 3y^2 + 6y + 6 \right) \right] \right\}.
    \label{eq:t}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Izmerite odvisnost svetlobnega toka halogene žarnice v razponu od rahlega žarenja do maksimalne moči. Pri tem merite tudi moč, ki se troši na žarnici.
    \item Narišite graf celotne izsevane moči kot funkcijo električne moči.
    \item Določite električno upornost žarnice kot funkcijo temperature.
    \item Narišite graf razmerja med zastrtim (s $\mathrm{Si}$ filtrom) in nemotenim svetlobnim tokom kot funkcijo temperature žarilne nitke.
\end{enumerate}

\section{Meritve in račun}

Žarnico postavimo na lestev pred bolometer, na razdaljo

\begin{equation*}
    d = (35 \pm 1)\,\si{cm}.
\end{equation*}

Prostorski kot, ki ga povkriva senzor lahko izrazimo s to razdaljo in površino senzorja $S$

\begin{equation*}
    \Omega = \frac{S}{d^2},
\end{equation*}

pri čemer je površina senzorja v bolometru $S = \SI{1}{cm^2}$. Če zdaj pomerimo odvisnost zaznane moči $P'(P_e)$ od električne moči, je celotna, v prostor izsevana moč žarnice, preprosto $P = P'/\Omega$. Na sliki~\ref{fig:P-by-P_e} imamo celotno izsevano moč $P$ v odvisnosti od električne moči, s katero napajamo žarnico. Skozi meritve prilagojeni premici imata naklona

\begin{figure}
    \centering
    \includegraphics{P-by-P_e.pdf}
    \caption{Celotna v prostor izsevana moč žarnice pri različnih eletričnih močeh $P_e$.}
    \label{fig:P-by-P_e}
\end{figure}


\begin{align*}
    k_1 &= 0.092 \pm 0.002, \\
    k_2 &= 0.034 \pm 0.001,
\end{align*}

od katerih je $k_1$ preprosto izkoristek te žarnice. Ker smo za vsako vrednost $P_e$ poleg izsevane moči $P$ izmerili tudi tok in napetost na žarnici, lahko preko Stefan-Boltzmanovega zakona povežemo upornost žarilne žičke s temperaturo žičke kot črnega telesa (slika~\ref{fig:R-by-T}). Pri tem smo morali upoštevati podatek, da je pri nazivni moči $\SI{30}{W}$ temperatura žarilne žičke

\begin{figure}
    \centering
    \includegraphics{R-by-T.pdf}
    \caption{Odvisnost upornosti žarilne nitke od temperature.}
    \label{fig:R-by-T}
\end{figure}

\begin{equation*}
    T_0 = (2700 \pm 100)\,\si{K}.
\end{equation*}

Upor žarilne žičke je za območje naših meritev linearno povezan s temperaturo kot

\begin{equation*}
    R(T) = k_T T + R_0,
\end{equation*}

z vrednostma

\begin{align*}
    k_T &= (0.56 \pm 0.01)\,\si{\ohm/K}, \\
    R_0 &= (160 \pm 20)\,\si{\ohm}.
\end{align*}

Končno preverimo še naš model za prepustnost silicijevega filtra, ki napoveduje modelsko funkcijo~\ref{eq:t}. Če jo prilagodimo na meritve~(\ref{fig:t-by-T}), dobimo

\begin{figure}
    \centering
    \includegraphics{t-by-T.pdf}
    \caption{Izmerjene vrednosti prevodnosti, prilagojena modelska krivulja (neprekinjena črta) in modelska krivulja, če ne bi upoštevali odbojev na površinah filtra (črtkana črta).}
    \label{fig:t-by-T}
\end{figure}

\begin{equation*}
    \eta = 0.52 \pm 0.02,
\end{equation*}

iz česar lahko po~(\ref{eq:n}) izračunamo lomni količnik silicija

\begin{equation*}
    n = 3.5 \pm 0.2.
\end{equation*}

\end{document}
