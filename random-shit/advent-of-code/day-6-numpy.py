import requests
import numpy as np

# Get the input file
day = 6
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
fish_ages = np.array([int(val) for val in input.split(',')])
adults = np.array(
    [np.sum(fish_ages == age) for age in range(7)])
younglings = np.zeros((9,))

def simulate(adults, younglings, N):
    '''Returns the fish population after N days.'''
    if N == 0:
        return sum(adults) + sum(younglings)
    newborns = adults[0] + younglings[0]
    adults = np.roll(adults, -1) # Age the grown-up fish by one
    adults[-1] += younglings[0] # The younglings have grown up
    younglings = np.roll(younglings, -1) # Age the younglings by one
    younglings[-1] = newborns # The newborns are now the youngest younglings
    return simulate(adults, younglings, N - 1)

print(simulate(adults, younglings, 80))
print(simulate(adults, younglings, 256))
