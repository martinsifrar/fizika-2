import requests

# Get the input file
day = 6
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
fish_ages = [int(val) for val in input.split(',')]
old = [list.count(fish_ages, age) for age in range(7)]
young = [0]*9

def simulate(old, young, N):
    '''Returns the fish population after N days. Fish are represented by two
    lists, one for old fish and one for young fish. Each list has the number of
    fish with the timer value i at its i-th index.'''
    if N == 0:
        return sum(old) + sum(young)
    newborn = old[0] + young[0]
    old = old[1:] + [old[0]] # Age the grown-up fish by one
    old[-1] += young[0] # The younglings have grown up
    young = young[1:] + [newborn] # Age the younglings by one, add newborns
    return simulate(old, young, N - 1)

print(simulate(old, young, 80))
print(simulate(old, young, 256))
