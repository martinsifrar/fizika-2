import requests


# Get the input file
day = 4
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
lines = [line for line in input.splitlines()]

empty_i = [i for i, line in enumerate(lines) if line == '']
game_nums = list(map(int, lines[0].split(sep=',')))
blocks = [lines[i+1:j] for i, j in zip(empty_i, empty_i[1:] + [len(lines)])]

def to_board(block):
    '''
    So we don't have to go through the whole board, we represent it as a
    3 touple of:
        - a dictionary {number: coordinate},
        - a list of the number of already marked numbers for each row,
        - a similar list but for columns.
    '''
    board_nums = dict([(int(s), (j, i))
          for j, row in enumerate(block)
          for i, s in enumerate(row.split())])
    row_track = [0]*5
    col_track = [0]*5
    return (board_nums, row_track, col_track)

def play_board(num, board):
    '''
    Play a num on a board and update the board. Returns a `is_bingo` flag
    and a new board tuple.
    '''
    board_nums, row_track, col_track = board
    is_bingo = False
    if (coords := board_nums.get(num)):
        j, i = coords
        row_track[j] += 1
        col_track[i] += 1
        board_nums.pop(num)
        if row_track[j] >= 5 or col_track[i] >= 5:
            is_bingo = True
    return is_bingo, (board_nums, row_track, col_track)

def split(num, boards):
    '''
    Splits boards into winning boards and the rest.
    '''
    winning, rest = [], []
    for i, board in enumerate(boards):
        is_bingo, new_board = play_board(num, board)
        if is_bingo:
            winning.append(new_board)
        else:
            rest.append(new_board)
    return winning, rest

boards = list(map(to_board, blocks))
for num in game_nums:
    winning_boards, rest = split(num, boards)
    boards = rest
    if len(winning_boards) >= 1:
        board_nums, _, _ = winning_boards[0]
        print(f'winning score: {num * sum(board_nums)}')
        break

boards = list(map(to_board, blocks))
for num in game_nums:
    winning_boards, rest = split(num, boards)
    boards = rest
    if len(rest) <= 0: # The only difference
        board_nums, _, _ = winning_boards[0]
        print(f'last winning score: {num * sum(board_nums)}')
        break
