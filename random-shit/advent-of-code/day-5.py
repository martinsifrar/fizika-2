import requests
import numpy as np

# Get the input file
day = 5
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
lines = [line for line in input.splitlines()]
parse_pt = lambda pt: [int(coord) for coord in pt.split(',')]
parse_line = lambda line: [parse_pt(pt) for pt in line.split(' -> ')]
pts = list(map(parse_line, lines))

def update(x, y, map, N):
    map[y,x] += 1
    if map[y,x] == 2:
        N += 1
    return map, N

map = np.zeros((1000, 1000))
N = 0 # Number of intersections
for pt in pts:
    (x_1, y_1), (x_2, y_2) = pt
    if x_1 == x_2:
        if y_2 < y_1:
            y_1, y_2 = y_2, y_1
        for y in range(y_1, y_2+1):
            map, N = update(x_1, y, map, N)
    elif y_1 == y_2:
        if x_2 < x_1:
            x_1, x_2 = x_2, x_1
        for x in range(x_1, x_2+1):
            map, N = update(x, y_1, map, N)
    else:
        if x_2 < x_1:
            (x_2, y_2), (x_1, y_1) = pt
        inc = 1 if y_2 > y_1 else -1
        for x, y in zip(range(x_1, x_2+1), range(y_1, y_2+inc, inc)):
            map, N = update(x, y, map, N)

print(N)
